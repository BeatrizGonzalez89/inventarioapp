## InventarioApp

Esta aplicación ha sido desarrollada por Beatriz González,
las herramientas que se utilizaron son las siguientes:

1. Java, Android nativo.
2. XML (Diseño de layout).
3. Android Studio.
4. Gestor de base de datos SQLite.

Descripción de aplicación: maneja entradas, procesos, existencias y salidas de los productos de una determinada tienda.