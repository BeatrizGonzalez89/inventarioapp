package com.example.david.utec.Producto;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.david.utec.Principal.SpinnerObjectString;
import com.example.david.utec.Principal.menu;
import com.example.david.utec.Proveedor.RowReporteA;
import com.example.david.utec.Proveedor.modificarElimiarProveedor;
import com.example.david.utec.Proveedor.proveedor;
import com.example.david.utec.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import db_gestion.BaseDeDatos;
import db_gestion.GestionDB;

public class Producto extends FragmentActivity {

    public SQLiteDatabase conexOpen;
    public Context context = this;
    public GestionDB objGestionDB ;
    public GestionDB obj;

    String nombreusuario;
    String id_proveedor, id_estado;
    EditText e_nombre_producto, e_codigo_producto;
    Spinner sp_proveedor, sp_estado;

   public  Context contexto= this;

    Dialog customDialog=null;


    // para la tabla
    private TableLayout mTableLayoutReporte;
    private ArrayList<ReporteProductos> mListaActividades;
    private LinearLayout mLinearLayoutContenedorPrincipal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_producto);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set content view AFTER ABOVE sequence (to avoid crash)
        this.setContentView(R.layout.activity_producto);

        Bundle bundle 	= getIntent().getExtras();
        nombreusuario   = bundle.getString("var_user");

       // Toast.makeText(this,"-->"+nombreusuario,Toast.LENGTH_LONG).show();
        e_nombre_producto=(EditText)findViewById(R.id.editTextNombreproducto);
        e_codigo_producto=(EditText)findViewById(R.id.editTextcodigoproducto);

        mTableLayoutReporte=				(TableLayout)findViewById(R.id.tablelayout_productos);


        BaseDeDatos objBaseDeDatos = new BaseDeDatos(this);
        try {
            objBaseDeDatos.createDataBase();
            // db2.openDataBase();
            //this.conexOpen = db2.myDataBase;
            this.objGestionDB = new GestionDB();// creo el objeto de la clase que gestiona la DB
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        CrearConsulta();

       // sp_sexo=(Spinner) findViewById(R.id.spinnerSexpaciente);
        sp_estado=(Spinner) findViewById(R.id.spinnerEstadoProducto);
        sp_proveedor=(Spinner) findViewById(R.id.spinnerproveedor);
        cargarspinerestado();
        cargarspinerproveedor();

        // SACAR ID DEL COMBO SEXO SELECCIONADO
        sp_estado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // guardo en variable el id del depto seleccionado
                id_estado = (((SpinnerObjectString) sp_estado.getSelectedItem()).getCodigo());
                //loadSpinnerDataSp_municipio(id_depto);// llamo al metodo que va a cargar los municipios
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        // SACAR ID DEL COMBO SEXO SELECCIONADO
        sp_proveedor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // guardo en variable el id del depto seleccionado
                id_proveedor = (((SpinnerObjectString) sp_proveedor.getSelectedItem()).getCodigo());
                //loadSpinnerDataSp_municipio(id_depto);// llamo al metodo que va a cargar los municipios
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });


              //  nombre=(EditText) findViewById(R.id.editTextNproveedor);


    }

    public void CrearConsulta(){

        mListaActividades=objGestionDB.consultaProductos(contexto);
        crearTabla();
    }

    private void crearTabla(){

        //Removemos el contenido anterior de la tabla en caso de que exista
        mTableLayoutReporte.removeAllViews();  //removeAllViewsInLayout ()
        mTableLayoutReporte.removeAllViewsInLayout();
        //Colocamo el nombre de la seleccion
        // mTextViewNombreSeleccion.setText(mNombreSeleccion)
//;
        for (ReporteProductos fila : mListaActividades) {

            //Creamos una nueva instancica de fila para la tabla
            final TableRow tableRow = new TableRow(context);
            // ListView listview = new ListView(context);

            //  tableRow.width = 252;

            //Creamos el texview donde se mostrara la informacion
            TextView textViewNombreproduto= new TextView(context);
            TextView textViewcodigo       = new TextView(context);
            TextView textViewproveedor    = new TextView(context);
            TextView textViewEstado     = new TextView(context);
            TextView id                 = new TextView(context);


            //Colocamos la nombre a mostrar en la columna
            textViewNombreproduto.setText((fila.getnombre()));
            textViewcodigo.setText((fila.getcodigo_producto()));
            textViewproveedor.setText(fila.getproveedor());

            textViewEstado.setText(fila.getestado());
            id.setText((fila.getid()));

            /*
               COLUMNA NOMBRE PRODUCTO
            */

            //Asignamos el color de backgroud que tendra la primer columna de la tabla
            textViewNombreproduto.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewNombreproduto.setGravity(Gravity.CENTER);
            textViewNombreproduto.setTextSize(15);
            textViewNombreproduto.setTypeface(null, Typeface.BOLD);
            textViewNombreproduto.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams nombreproductoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            nombreproductoTableRowParams.setMargins(1, 1, 1, 1);
            nombreproductoTableRowParams.width = 97;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewNombreproduto, nombreproductoTableRowParams);

            // FIN COLUMNA NOMBRE


                    /*
                    COLUMNA CODIGO
                     */
            //Asignamos el color
            textViewcodigo.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewcodigo.setGravity(Gravity.CENTER);
            textViewcodigo.setTextSize(15);
            textViewcodigo.setTypeface(null, Typeface.BOLD);
            textViewcodigo.setHeight(50);


            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams codigoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            codigoTableRowParams.setMargins(1, 1, 1, 1);
            codigoTableRowParams.width = 101;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewcodigo, codigoTableRowParams);

            //FIN COLUMNA CODIGO.

                /*
                COLUMNA PROVEEDOR
                 */
            textViewproveedor.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));


            textViewproveedor.setGravity(Gravity.CENTER);
            textViewproveedor.setTextSize(15);
            textViewproveedor.setTypeface(null, Typeface.BOLD);
            textViewproveedor.setHeight(50);

            TableRow.LayoutParams proveedorTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            proveedorTableRowParams.setMargins(1, 1, 1, 1);
            proveedorTableRowParams.width = 101;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewproveedor, proveedorTableRowParams);

            // FIN COLUMNA PROVEEDOR









            /*
            columna  ESTADO.
             */
            textViewEstado.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewEstado.setGravity(Gravity.CENTER);
            textViewEstado.setTextSize(15);
            textViewEstado.setTypeface(null, Typeface.BOLD);
            textViewEstado.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams estadoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            estadoTableRowParams.setMargins(1, 1, 1, 1);
            estadoTableRowParams.width = 100;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewEstado, estadoTableRowParams);
            // columna estado.




            //Creamos el linear layout que contendra las actividades de la fecha
            LinearLayout layout = new LinearLayout(context);
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));






            // COLUMNA ALIAS


            //Agregamos las actividades a la fila de la tabla para que utilicen la segunda columna
            // tableRow.addView(layout, activdadTableRowParams);
            final String s_id 	    = id.getText().toString().trim();
            tableRow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {


                    tableRow.setBackgroundColor(getResources().getColor(android.R.color.holo_green_light));


                    Intent i = new Intent(Producto.this, modificarEliminarProducto.class);
                    i.putExtra("s_id",s_id);
                    i.putExtra("var_user" ,nombreusuario);
                    startActivity(i);





                }
            });

            //Agregamos la fila creada a la tabla de la pantalla

            mTableLayoutReporte.addView(tableRow);

        }

    }


    public void cargarspinerestado(){
        // CARGAR COMBO DE ESTADO --> SPINNER

        List<SpinnerObjectString> lables = objGestionDB.getEstado(this.contexto);
        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(this, android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_estado.setAdapter(dataAdapter);



    }

    public void cargarspinerproveedor(){
        // CARGAR COMBO PROVEEDOR --> SPINNER

        List<SpinnerObjectString> lables = objGestionDB.getProveedor(this.contexto);
        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(this, android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_proveedor.setAdapter(dataAdapter);



    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_producto, menu);
        return true;
    }


            public void click_buscarProductos(View buscarproducto){

                String  s_nombre_producto= e_nombre_producto.getText().toString().trim();
                String  s_codigo_producto= e_codigo_producto.getText().toString().trim();

                int respueta = validarbuscar(s_nombre_producto, s_codigo_producto, id_proveedor, id_estado);
                if(respueta==0){

                    mTableLayoutReporte.removeAllViews();
                    mListaActividades=objGestionDB.consultaProductosbusqueda(contexto, s_nombre_producto,s_codigo_producto,id_proveedor,id_estado);
                    crearTabla();

               }else {

                    informar();
                }

            }

    public void informar()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.mensajeinformacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloErrorUsuario);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeErrorUsuario);
        contenido.setText("Debe de Ingresar al menos un Parámetro, Para Realizar la Búsqueda..");

        ((Button) customDialog.findViewById(R.id.btn_aceptarErrorUsuario)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });



        customDialog.show();
    }

        public void click_guardarProducto(View guardar_producto){

            String s_nombre_producto= e_nombre_producto.getText().toString().trim();
            String s_codigo_producto= e_codigo_producto.getText().toString().trim();

           int respuesta= validar(s_nombre_producto, s_codigo_producto, id_proveedor, id_estado);

            if(respuesta==0){
               // Toast.makeText(this,"bien", Toast.LENGTH_SHORT).show();




                objGestionDB.insertProducto(contexto, s_nombre_producto, s_codigo_producto, id_proveedor, id_estado);
                confirmacion();

            }else {
               // Toast.makeText(this, "complete", Toast.LENGTH_SHORT).show();


                errorcompletarcampo();

            }

           // Toast.makeText(this,"-->"+id_proveedor+"estado-->"+id_estado,Toast.LENGTH_SHORT).show();

        }

    public void  confirmacion(){

        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.confirmacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloconfirmacion);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeconfirmacion);
        contenido.setText("Producto Ingresado Correctamente");

        ((Button) customDialog.findViewById(R.id.btn_aceptarConfirmacion)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();
                // i.putExtra("var_user",nombreusuario);


                Intent i= new Intent(Producto.this,Producto.class);
                i.putExtra("var_user",nombreusuario);
                finish();
                startActivity(i);

            }
        });



        customDialog.show();

    }

    public void errorcompletarcampo()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.errorcompletecampos);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloErrorUsuario);
        titulo.setText("ERROR!!");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeErrorUsuario);
        contenido.setText("Complete los Campos");

        ((Button) customDialog.findViewById(R.id.btn_aceptarErrorUsuario)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });

	    /*((Button) customDialog.findViewById(R.id.cancelar)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view)
			{
				customDialog.dismiss();
				//Toast.makeText(MainActivity.this, R.string.cancelar, Toast.LENGTH_SHORT).show();

			}
		});*/

        customDialog.show();
    }


            public int validar (String s_nombre_producto, String  s_codigo_producto, String id_proveedor, String id_estado ){

              int   respuesta=0;

                if (s_nombre_producto.equals("")) {
                    return respuesta = 1;

                }

                if(s_codigo_producto.equals("")){

                    return respuesta=1;

                }

                if(id_proveedor.equals("0")){
                    return respuesta=1;

                }

                if(id_estado.equals("0")){
                    return respuesta=1;

                }


                return respuesta;

            }


    public int validarbuscar (String s_nombre_producto, String  s_codigo_producto, String id_proveedor, String id_estado ){

        int   respuesta=0;

        if ( (s_nombre_producto.equals("") )  && (s_codigo_producto.equals("")   )  && (id_proveedor.equals("0"))  &&  (id_estado.equals("0")) ) {
            return respuesta = 1;

        }




        return respuesta;

    }


    public void retornarmenu( View menu_principal){

        Intent i = new Intent(this, menu.class);
        i.putExtra("var_user",nombreusuario);
        finish();
        startActivity(i);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
