package com.example.david.utec.Detalle;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.david.utec.Principal.SpinnerObjectString;
import com.example.david.utec.R;
import com.example.david.utec.Salidas.Salidas;

import java.io.IOException;
import java.util.List;

import db_gestion.BaseDeDatos;
import db_gestion.GestionDB;

public class Modificar_Detalle extends Activity {

    public SQLiteDatabase conexOpen;
    public Context context = this;
    public GestionDB objGestionDB ;
    public GestionDB obj;


    Dialog customDialog = null;

    public Context contexto = this;

    String id_detalle, nombre_usuario, id_cliente, id_proveedor, id_producto;

    EditText proveedor,  cliente, cantidad;

    TextView  tex_cliente, tex_proveedor,  tex_producto;

    Spinner sp_lote;

    int id_lote, n_id_lote=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  setContentView(R.layout.activity_modificar__detalle);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set content view AFTER ABOVE sequence (to avoid crash)
        // this.setContentView(R.layout.activity_detalle);


        Bundle bundle= getIntent().getExtras();
        id_detalle=bundle.getString("s_id");
        nombre_usuario=bundle.getString("var_user");
        id_proveedor=bundle.getString("id_proveedor");
        id_producto=bundle.getString("id_producto");
        id_cliente=bundle.getString("idcliente");

         Toast.makeText(this,id_detalle,Toast.LENGTH_SHORT).show();

        final BaseDeDatos objBaseDeDatos = new BaseDeDatos(this);
        try {
            objBaseDeDatos.createDataBase();
            // db2.openDataBase();
            //this.conexOpen = db2.myDataBase;
            this.objGestionDB = new GestionDB();// creo el objeto de la clase que gestiona la DB
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }





        objGestionDB.detalleProveedor(contexto, id_proveedor);
        objGestionDB.detalleProducto(contexto, id_producto);
        objGestionDB.detalleCliente(contexto, id_cliente);
        objGestionDB.detallemodificar(id_detalle, contexto);

        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.activity_modificar__detalle);












        sp_lote=(Spinner)customDialog.findViewById(R.id.spinnerlotedetalle_modificar);
        cargarspinerlote();

        sp_lote.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // guardo en variable el id del depto seleccionado
                id_lote = (((SpinnerObjectString) sp_lote.getSelectedItem()).getId());


            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }

        });


        // cerrar -- regresar al detalle
        ((ImageView) customDialog.findViewById(R.id.ima_cerrar_detalle_modificar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.cancelar, Toast.LENGTH_SHORT).show();


                Intent i = new Intent(Modificar_Detalle.this, Detalle.class);
                i.putExtra("var_user", nombre_usuario);
                i.putExtra("id_proveedor", id_proveedor);
                i.putExtra("id_producto", id_producto);
                i.putExtra("idcliente", id_cliente);
                finish();
                startActivity(i);

            }
        });

        // recargar -- regresar al detalle
        ((ImageView) customDialog.findViewById(R.id.imagemenudetalle)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.cancelar, Toast.LENGTH_SHORT).show();


                Intent i = new Intent(Modificar_Detalle.this, Detalle.class);
                i.putExtra("var_user", nombre_usuario);
                i.putExtra("id_proveedor", id_proveedor);
                i.putExtra("id_producto", id_producto);
                i.putExtra("idcliente", id_cliente);
                finish();
                startActivity(i);

            }
        });


        // Modificar  detalle
        ((ImageView) customDialog.findViewById(R.id.imageModificardetalle)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

               modificardetalle();

            }
        });

        tex_proveedor =(TextView) customDialog.findViewById(R.id.text_proveedordetalle_modifiar);
        tex_producto =(TextView) customDialog.findViewById(R.id.text_productodetalle_modificar);
        tex_cliente=(TextView)customDialog.findViewById(R.id.text_cliente_detalle_modificar);
        cantidad =(EditText)customDialog.findViewById(R.id.editTextcantidadproductodetalle_modificar);
        //text_cliente

        tex_cliente.setText(objGestionDB.cliente_detalle);
        tex_proveedor.setText(objGestionDB.proveedor_detalle);
        tex_producto.setText(objGestionDB.producto_detalle);
        cantidad.setText(objGestionDB.cantidad);








        customDialog.show();
    }


    // CARGAR COMBO DE EXÁMEN --> SPINNER
    public void cargarspinerlote() {



         objGestionDB.detallemodificar(id_detalle, contexto);

        n_id_lote=objGestionDB.id_lotemodificar;




      //   Toast.makeText(this, "id del lote"+n_id_lote  , Toast.LENGTH_LONG).show();
       // int lote=objGestionDB.lote(id_detalle, contexto);

       // int existencia= objGestionDB.vercantidaddetalle(contexto, id_producto, lote);

       // Toast.makeText(this, "<<>>"+existencia  , Toast.LENGTH_LONG).show();


        List<SpinnerObjectString> lables = objGestionDB.getLotemodificar(this.contexto, id_producto, n_id_lote);

        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(this, android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_lote.setAdapter(dataAdapter);

        int pos=0;
        for(int i=0; i<lables.size(); i++){
            if(lables.get(i).getId()==n_id_lote){
                pos=i;
            }
        }
        sp_lote.setSelection(pos);
    }



    public void modificardetalle(){

        String s_cantidad= cantidad.getText().toString().trim();

      int respuesta=  validar (s_cantidad, id_lote);

        if (respuesta==0){
            //Toast.makeText(this,"bien",Toast.LENGTH_SHORT).show();

                            int existencia= objGestionDB.vercantidaddetalle(contexto, id_producto, id_lote);

                            int cantidad = Integer.parseInt(s_cantidad);

                           // int cantidadregistrada=objGestionDB.cantidad(id_detalle, contexto);

                           // Toast.makeText(this,""+cantidad+" "+existencia,Toast.LENGTH_SHORT).show();


                            if (cantidad<=existencia){
                               // Toast.makeText(this,"ok",Toast.LENGTH_SHORT).show();
                                     objGestionDB.actualizarDetalle(contexto,cantidad,id_detalle);
                                    confirmacion();
                                }
                        else{
                                    informar();
                            }



        }else {

            errorcompletarcampo();

        }
    }


    public int validar (String s_cantidad, int  id_lote ){
        int respuesta=0;

        if(s_cantidad.equals("") || id_lote==0){

            respuesta=1;
        }

        return respuesta;
    }



    public void  confirmacion(){

        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.confirmacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloconfirmacion);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeconfirmacion);
        contenido.setText("Detalle Actualizado Correctamente");

        ((Button) customDialog.findViewById(R.id.btn_aceptarConfirmacion)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();
                // i.putExtra("var_user",nombreusuario);


                Intent i = new Intent(Modificar_Detalle.this, Detalle.class);
                i.putExtra("var_user", nombre_usuario);
                i.putExtra("id_proveedor", id_proveedor);
                i.putExtra("id_producto", id_producto);
                i.putExtra("idcliente", id_cliente);
                finish();
                startActivity(i);







            }
        });



        customDialog.show();

    }


    public void errorcompletarcampo()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.errorcompletecampos);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloErrorUsuario);
        titulo.setText("ERROR!!");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeErrorUsuario);
        contenido.setText("Complete los Campos");

        ((Button) customDialog.findViewById(R.id.btn_aceptarErrorUsuario)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });



        customDialog.show();
    }


    public void informar()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.mensajeinformacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloErrorUsuario);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeErrorUsuario);
        contenido.setText("Cantidad seleccionada es mayor a la cantidad con la que el lote dispone..");

        ((Button) customDialog.findViewById(R.id.btn_aceptarErrorUsuario)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });



        customDialog.show();
    }

}
