/**
 *Esta clase representa los datos dde una fila
 *de la tabla de actividades del reporte A 
 */
package com.example.david.utec.Existencias;

import java.util.ArrayList;

public class ReporteExistencias {
	public String proveedor;
	public String nombre_producto;
	public String cantidad;
	public String lote;
	public String fechavencimiento;
	public String id;



	// NOMBRE del CLIENTE

	public String getproveedor() {
		return proveedor;
	}
	public void setproveedor(String proveedor) {
		proveedor = proveedor;
	}



	// NOMBRE del producto

	public String getnombre() {
		return nombre_producto;
	}
	public void setnombre(String nombre_producto) {
		nombre_producto = nombre_producto;
	}


		// cantidad
	public String getcantidad() {
		return cantidad;
	}
	public void setcantidad(String cantidad) {
		cantidad = cantidad;
	}


	// lote
	public String getlote() {
		return lote;
	}
	public void setlote(String lote) {
		lote = lote;
	}

		// fecha vencimiento
	public String getfechavencimiento() {
		return fechavencimiento;
	}
	public void setfechavencimiento(String fechavencimiento) {
		fechavencimiento = fechavencimiento;
	}





	// id detalle
	public String getid() {
		return id;
	}
	public void setid(String id) {
		id = id;
	}
	


	

}
