package com.example.david.utec.Principal;

public class SpinnerObjectString {

	private String databaseCodigo;
	private String databaseValue;
	private int databaseId;

	public SpinnerObjectString(String databaseCodigo, String databaseValue) {
		this.databaseCodigo = databaseCodigo;
		this.databaseValue = databaseValue;
	}
	public SpinnerObjectString(int databaseId, String databaseValue) {
		this.databaseId = databaseId;
		this.databaseValue = databaseValue;
	}
	
	
	public String getCodigo() {
		return databaseCodigo;
	}
	public int getId() {
		return databaseId;
	}
	
	public String getValue() {
		return databaseValue;
	}

	@Override
	public String toString() {
		return databaseValue;
	}
}
