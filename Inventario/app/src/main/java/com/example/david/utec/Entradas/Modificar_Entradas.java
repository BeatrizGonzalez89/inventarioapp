package com.example.david.utec.Entradas;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;



import com.example.david.utec.Principal.*;
import com.example.david.utec.Entradas.*;


import android.support.v4.app.Fragment;


import com.example.david.utec.Proveedor.RowReporteA;
import com.example.david.utec.Proveedor.modificarElimiarProveedor;
import com.example.david.utec.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import db_gestion.BaseDeDatos;
import db_gestion.GestionDB;

//public class Entradas extends Activity implements OnClickListener {
public class Modificar_Entradas extends Activity implements OnClickListener{

    public SQLiteDatabase conxOpen;
    public Context contexto= this;
    public Context context = this;
    public GestionDB objGestionDB;
    public  GestionDB obj;

    Dialog customDialog= null;
    String id_entrada, nombre_usuario;
    public ImageButton ibutton_fecha_vencimiento;

    private EditText cantidad_producto ,fecha_vencimiento;

    Spinner sp_proveedor, sp_producto;

    int id_proveedor, n_id_proveedor=0,  id_producto, n_id_producto=0;

    // para la tabla
    private TableLayout mTableLayoutReporte;
    private ArrayList<TablaEntradas> mListaActividades;
    private LinearLayout mLinearLayoutContenedorPrincipal;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_entradas);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set content view AFTER ABOVE sequence (to avoid crash)
        this.setContentView(R.layout.activity_modificar__entradas);








        Bundle bundle= getIntent().getExtras();
        id_entrada=bundle.getString("s_id");
        nombre_usuario=bundle.getString("var_user");

        Toast.makeText(this, id_entrada + nombre_usuario, Toast.LENGTH_SHORT).show();

     //   ibutton_fecha_vencimiento = (ImageButton) findViewById(R.id.imageFechavencimiento);
        fecha_vencimiento = (EditText) findViewById(R.id.txt_fecha_vencimiento);
        cantidad_producto= (EditText)findViewById(R.id.editTextcantidadproducto);
       // ibutton_fecha_vencimiento.setOnClickListener(this);


        ibutton_fecha_vencimiento = (ImageButton) findViewById(R.id.imageFechavencimientomodificar);

        ibutton_fecha_vencimiento.setOnClickListener(this);
        // fecha_nacimiento.setFocusable(false);

        ibutton_fecha_vencimiento = (ImageButton) findViewById(R.id.imageFechavencimientomodificar);




        mTableLayoutReporte=				(TableLayout)findViewById(R.id.tablelayout_entradas_moficar);






        BaseDeDatos objBaseDeDatos = new BaseDeDatos(this);
        try {
            objBaseDeDatos.createDataBase();
            // db2.openDataBase();
            //this.conexOpen = db2.myDataBase;
            this.objGestionDB = new GestionDB();// creo el objeto de la clase que gestiona la DB
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        CrearConsulta();

        cantidad_producto=(EditText) findViewById(R.id.editTextcantidadproductomodificar);
        fecha_vencimiento=(EditText)findViewById(R.id.txt_fecha_vencimientomodificar);

        sp_proveedor=(Spinner)findViewById(R.id.spinnerproveedorentradasmodificar);
        sp_producto=(Spinner)findViewById(R.id.spinnerproductoentradasmodificar);


        // MOSTRAR LO QUE CONTIENE LAS VARIABLE
        objGestionDB.EditarEntradas(contexto, id_entrada);
        cantidad_producto.setText(objGestionDB.cantidad_producto);
        fecha_vencimiento.setText(objGestionDB.fecha_vencimiento);


        cargarspinerProveedor(id_entrada);

        // SACAR ID ESTADO SELECCIONADO
        sp_proveedor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // guardo en variable el id de lo seleccionado
                id_proveedor = (((SpinnerObjectString) sp_proveedor.getSelectedItem()).getId());
                cargarspinerproducto(id_entrada, id_proveedor);
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });



        //cargarspinerproducto(id_entrada);

        // SACAR ID ESTADO SELECCIONADO
        sp_producto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // guardo en variable el id de lo seleccionado
                id_producto = (((SpinnerObjectString) sp_producto.getSelectedItem()).getId());

            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });





        //  Toast.makeText(this,"lote"+lote,Toast.LENGTH_SHORT).show();








    }

    public void CrearConsulta(){

        mListaActividades=objGestionDB.consultaentradas(contexto);
        crearTabla();
    }

    private void crearTabla(){

        //Removemos el contenido anterior de la tabla en caso de que exista
        mTableLayoutReporte.removeAllViews();  //removeAllViewsInLayout ()
        mTableLayoutReporte.removeAllViewsInLayout();
        //Colocamo el nombre de la seleccion
        // mTextViewNombreSeleccion.setText(mNombreSeleccion)
//;
        for (TablaEntradas fila : mListaActividades) {

            //Creamos una nueva instancica de fila para la tabla
            final TableRow tableRow = new TableRow(context);
            // ListView listview = new ListView(context);

            //  tableRow.width = 252;

            //Creamos el texview donde se mostrara la informacion
            TextView textViewNombreCompleto      = new TextView(context);
            TextView textViewSexo       = new TextView(context);
            TextView textViewAlias      = new TextView(context);
            TextView textViewDireccion  = new TextView(context);
            TextView textViewTelefono   = new TextView(context);
            TextView textViewEstado     = new TextView(context);
            TextView id                 = new TextView(context);


            //Colocamos la nombre a mostrar en la columna
            textViewNombreCompleto.setText((fila.getproveedor()));
            textViewSexo.setText((fila.getproducto()));
            textViewAlias.setText(fila.getcantidad_producto());
            textViewDireccion.setText(fila.getfechavencimiento());
            textViewTelefono.setText(fila.getfecha());
            textViewEstado.setText(fila.getestado());
            id.setText((fila.getid()));

            /*
               COLUMNA NOMBRE COMPLETO
            */

            //Asignamos el color de backgroud que tendra la primer columna de la tabla
            textViewNombreCompleto.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewNombreCompleto.setGravity(Gravity.CENTER);
            textViewNombreCompleto.setTextSize(15);
            textViewNombreCompleto.setTypeface(null, Typeface.BOLD);
            textViewNombreCompleto.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams nombreCompletoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            nombreCompletoTableRowParams.setMargins(1, 1, 1, 1);
            nombreCompletoTableRowParams.width = 99;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewNombreCompleto, nombreCompletoTableRowParams);

            // FIN COLUMNA NOMBRE


                    /*
                    COLUMNA SEXO
                     */
            //Asignamos el color
            textViewSexo.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewSexo.setGravity(Gravity.CENTER);
            textViewSexo.setTextSize(15);
            textViewSexo.setTypeface(null, Typeface.BOLD);
            textViewSexo.setHeight(50);


            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams SexoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            SexoTableRowParams.setMargins(1, 1, 1, 1);
            SexoTableRowParams.width = 200;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewSexo, SexoTableRowParams);

            //FIN COLUMNA SEXO.

                /*
                COLUMNA ALIAS
                 */
            textViewAlias.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));


            textViewAlias.setGravity(Gravity.CENTER);
            textViewAlias.setTextSize(15);
            textViewAlias.setTypeface(null, Typeface.BOLD);
            textViewAlias.setHeight(50);

            TableRow.LayoutParams AliasTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            AliasTableRowParams.setMargins(1, 1, 1, 1);
            AliasTableRowParams.width = 100;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewAlias, AliasTableRowParams);

            // FIN COLUMNA ALIAS


            /*
            COLUMNA DIRECCION
             */
            //Asignamos el color de backgroud que tendra la primer columna de la tabla
            textViewDireccion.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewDireccion.setGravity(Gravity.CENTER);
            textViewDireccion.setTextSize(15);
            textViewDireccion.setTypeface(null, Typeface.BOLD);
            textViewDireccion.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams direccionTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            direccionTableRowParams.setMargins(1, 1, 1, 1);
            direccionTableRowParams.width = 210;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewDireccion, direccionTableRowParams);
            // FIN COLUMNA DIRECCION


            /*
            columna telefono
             */
            textViewTelefono.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewTelefono.setGravity(Gravity.CENTER);
            textViewTelefono.setTextSize(15);
            textViewTelefono.setTypeface(null, Typeface.BOLD);
            textViewTelefono.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams telefonoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            telefonoTableRowParams.setMargins(1, 1, 1, 1);
            telefonoTableRowParams.width = 110;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewTelefono, telefonoTableRowParams);
            // fin columna telefono.




            /*
            columna  ESTADO.
             */
            textViewEstado.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewEstado.setGravity(Gravity.CENTER);
            textViewEstado.setTextSize(15);
            textViewEstado.setTypeface(null, Typeface.BOLD);
            textViewEstado.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams estadoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            estadoTableRowParams.setMargins(1, 1, 1, 1);
            estadoTableRowParams.width = 120;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewEstado, estadoTableRowParams);

            // columna estado.




            //Creamos el linear layout que contendra las actividades de la fecha
            LinearLayout layout = new LinearLayout(context);
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));











            //Agregamos las actividades a la fila de la tabla para que utilicen la segunda columna
            // tableRow.addView(layout, activdadTableRowParams);
            final String s_id 	    = id.getText().toString().trim();
            tableRow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {


                    tableRow.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_light));
                    // Toast.makeText(this, "---->"+s_id , Toast.LENGTH_LONG).show();
                    Intent i = new Intent(Modificar_Entradas.this, modificar_eliminar_entradas.class);
                    i.putExtra("s_id", s_id);
                    i.putExtra("var_user",nombre_usuario);
                    // finish();
                    startActivity(i);


                    // modificarEliminar(s_id);



                }
            });

            //Agregamos la fila creada a la tabla de la pantalla

            mTableLayoutReporte.addView(tableRow);

        }

    }





    public void click_modificarentradas(View modificar){

        String s_fecha_vencimiento=   fecha_vencimiento.getText().toString().trim();
        String s_cantidad=  cantidad_producto.getText().toString().trim();

        int respuesta= validar (s_fecha_vencimiento,s_cantidad, id_proveedor, id_producto);

        if(respuesta==0){

            Toast.makeText(this, "bien", Toast.LENGTH_SHORT).show();

            objGestionDB.actualizarEntrada(contexto, s_fecha_vencimiento,s_cantidad, id_proveedor, id_producto, id_entrada);
            confirmacion();

        }else{
            Toast.makeText(this, "complete", Toast.LENGTH_SHORT).show();
            errorcompletarcampo();
        }

    }

    public  int validar (String s_fecha_vencimiento , String s_cantidad , int id_proveedor , int id_producto){
        int respuesta=0;

        if (   (s_fecha_vencimiento.equals("")) || (s_cantidad.equals(""))   ||  (id_proveedor==99) || (id_producto==99)    ){

            respuesta=1;
        }
        return respuesta;
    }




    public void  click_recargarentradas(View retornar){

        Intent i= new Intent(this, Entradas.class);
        i.putExtra("var_user", nombre_usuario);
        finish();
        startActivity(i);


    }


    // CARGAR COMBO DE estado --> SPINNER
    public void cargarspinerProveedor(String id_entrada) {



        objGestionDB.EditarEntradas(contexto, id_entrada);
        n_id_proveedor=objGestionDB.id_proveedorentradas;
        // Toast.makeText(this, "id del examen"+n_id_examen  , Toast.LENGTH_LONG).show();


        List<SpinnerObjectString> lables = objGestionDB.geteEditeproveedor(contexto);

        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(this, android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_proveedor.setAdapter(dataAdapter);

        int pos=0;
        for(int i=0; i<lables.size(); i++){
            if(lables.get(i).getId()==n_id_proveedor){
                pos=i;
            }
        }
        sp_proveedor.setSelection(pos);
    }


    // CARGAR COMBO DE estado --> SPINNER
    public void cargarspinerproducto(String id_entrada, int  id_proveedor) {



        objGestionDB.EditarEntradas(contexto, id_entrada);
        n_id_producto=objGestionDB.id_productoentradas;

        // Toast.makeText(this,""+id_proveedor, Toast.LENGTH_SHORT).show();


        List<SpinnerObjectString> lables = objGestionDB.geteEditeproducto(contexto, id_proveedor);

        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(this, android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_producto.setAdapter(dataAdapter);

        int pos=0;
        for(int i=0; i<lables.size(); i++){
            if(lables.get(i).getId()==n_id_producto){
                pos=i;
            }
        }
        sp_producto.setSelection(pos);
    }






    public void informar()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.mensajeinformacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloErrorUsuario);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeErrorUsuario);
        contenido.setText("Debe de Ingresar al menos un Parámetro, Para Realizar la Búsqueda..");

        ((Button) customDialog.findViewById(R.id.btn_aceptarErrorUsuario)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });



        customDialog.show();
    }


    public void errorcompletarcampo()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.errorcompletecampos);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloErrorUsuario);
        titulo.setText("ERROR!!");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeErrorUsuario);
        contenido.setText("Complete los Campos");

        ((Button) customDialog.findViewById(R.id.btn_aceptarErrorUsuario)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });



        customDialog.show();
    }


    public void  confirmacion(){

        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.confirmacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloconfirmacion);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeconfirmacion);
        contenido.setText("La Entrada al Producto a sido Actualizada Correctamente");

        ((Button) customDialog.findViewById(R.id.btn_aceptarConfirmacion)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();
                // i.putExtra("var_user",nombreusuario);


                Intent i= new Intent(Modificar_Entradas.this,Entradas.class);
                i.putExtra("var_user",nombre_usuario);
                finish();
                startActivity(i);

            }
        });



        customDialog.show();

    }





    public void regresarentradasmenu(View regresar){

        Intent i= new Intent(this, menu.class);
        i.putExtra("var_user",nombre_usuario);
        finish();
        startActivity(i);

    }




    public void onClick(View v){
        showDialog(0);
    }
    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id){
        Calendar fechaActual = new GregorianCalendar();
        int anioActual = fechaActual.get(Calendar.YEAR);
        int mesoActual = fechaActual.get(Calendar.MONTH);
        int diaActual  = fechaActual.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(this, datePickerListener, anioActual, mesoActual, diaActual);
    }
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,int selectedMonth, int selectedDay) {


            //fechaActual;
            Calendar fechaActual = new GregorianCalendar();
            int anioActual = fechaActual.get(Calendar.YEAR);
            int mesoActual = fechaActual.get(Calendar.MONTH);
            int diaActual  = fechaActual.get(Calendar.DAY_OF_MONTH);

            String fechita= selectedYear+"-"+(selectedMonth + 1)+"-"+selectedDay;
            fecha_vencimiento.setText(fechita);





        }
    };






}
