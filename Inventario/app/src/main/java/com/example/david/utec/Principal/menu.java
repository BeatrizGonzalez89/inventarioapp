package com.example.david.utec.Principal;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.TextView;


import com.example.david.utec.Clientes.Clientes;
import com.example.david.utec.Existencias.Existencias;
import com.example.david.utec.Producto.Producto;
import com.example.david.utec.Proveedor.proveedor;
import  com.example.david.utec.Entradas.*;
import com.example.david.utec.R;
import com.example.david.utec.Salidas.Salidas;
import com.example.david.utec.Clave.*;
import com.example.david.utec.prueba_checkBox.checkBox;

import db_gestion.GestionDB;


public class menu extends FragmentActivity {

    TextView texto,usuario_menu;
    public SQLiteDatabase conexOpen;
    public Context contexto = this;
    public GestionDB objGestionDB;

    String nombreusuario;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /// setContentView(R.layout.menupricipal);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set content view AFTER ABOVE sequence (to avoid crash)
        this.setContentView(R.layout.menupricipal);

        Bundle bundle 	= getIntent().getExtras();
        nombreusuario   = bundle.getString("var_user");

     //   Toast.makeText(this, "---->"+nombreusuario , Toast.LENGTH_LONG).show();


        usuario_menu = (TextView)findViewById(R.id.text_usuario_menu);




        usuario_menu.setText(" "+nombreusuario);
    }



    public void cerrarsesion(View view) {

        Intent i = new Intent(this, MainActivity.class);
        //i.putExtra("var_user",s_username);
        finish();
        startActivity(i);
    }

    public void CrearProveedor(View view) {

        Intent i = new Intent(this, proveedor.class);
        i.putExtra("var_user", nombreusuario);
        finish();
        startActivity(i);
       // Toast.makeText(this, "PROVEEDOR" , Toast.LENGTH_LONG).show();
    }


    public void CrearProducto(View view) {

        Intent i = new Intent(this, Producto.class);
        i.putExtra("var_user",nombreusuario);
        finish();
        startActivity(i);
       // Toast.makeText(this, "PRODUCTO" , Toast.LENGTH_LONG).show();
    }

    public void CrearCliente(View clientes){

        Intent i = new Intent(this, checkBox.class);
        i.putExtra("var_user",nombreusuario);
        finish();
        startActivity(i);

    }

    public void Entradas(View view) {

        Intent i = new Intent(this, Entradas.class);
        i.putExtra("var_user",nombreusuario);
        finish();
        startActivity(i);


    }

    public void Salidas(View view) {

        Intent i = new Intent(this, Salidas.class);
        i.putExtra("var_user",nombreusuario);
        finish();
        startActivity(i);
    }

    public void Contra(View view) {

        Intent i = new Intent(this, cambio_de_clave.class);
        i.putExtra("var_user", nombreusuario);
        startActivity(i);
       // Toast.makeText(this, "CONTRA" , Toast.LENGTH_LONG).show();
    }


    public void existencias (View existencias){

       // Toast.makeText(this,"EXISTENCIAS...", Toast.LENGTH_SHORT).show();

        Intent i = new Intent(this, Existencias.class);
        i.putExtra("var_user",nombreusuario);
        finish();
        startActivity(i);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_otro, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
