package com.example.david.utec.Proveedor;

import android.app.Dialog;
import android.media.Image;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.david.utec.Principal.MainActivity;
import com.example.david.utec.Principal.SpinnerObject;
import com.example.david.utec.R;
import com.example.david.utec.Principal.menu;

import com.example.david.utec.Principal.SpinnerObjectString;

import db_gestion.BaseDeDatos;
import db_gestion.GestionDB;


import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import java.io.IOException;
import java.util.List;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.LinearLayout;
import android.widget.ArrayAdapter;
import java.util.ArrayList;
import android.view.Gravity;
import android.graphics.Typeface;
import android.view.ViewGroup.LayoutParams;

public class Modificar_Proveedor extends FragmentActivity {

    public SQLiteDatabase conexOpen;
    public Context context = this;
    public GestionDB objGestionDB ;
    public GestionDB obj;

    //VARIABLES.
    private EditText nombre,apellido,direccion, telefono, sexo, alias;

    String s_id="";
    public Context contexto = this;
    String id_proveedor,nombreusuario;

    // variables para el campo sexo y estado
    Spinner sp_sexo,  sp_estado;
    int  id_sexo, id_estado,  n_id_sexo=0,  n_id_estado;

    // varibale para el dialogo
        Dialog customDialog=null;

    // para la tabla
    private TableLayout mTableLayoutReporte;
    private ArrayList<RowReporteA> mListaActividades;
    private LinearLayout mLinearLayoutContenedorPrincipal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_modificar__proveedor);


        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set content view AFTER ABOVE sequence (to avoid crash)
        this.setContentView(R.layout.activity_modificar__proveedor);

        Bundle bundle = getIntent().getExtras();
        id_proveedor= bundle.getString("s_id");
        nombreusuario   = bundle.getString("var_user");



      //  Toast.makeText(this, "--->" + id_proveedor+nombreusuario, Toast.LENGTH_LONG).show();

        BaseDeDatos objBaseDeDatos = new BaseDeDatos(this);
        try {
            objBaseDeDatos.createDataBase();
            // db2.openDataBase();
            //this.conexOpen = db2.myDataBase;
            this.objGestionDB = new GestionDB();// creo el objeto de la clase que gestiona la DB
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        nombre=(EditText) findViewById(R.id.editTextMNproveedor);
        apellido=(EditText)findViewById(R.id.editTextMAproveedor);
        telefono=(EditText)findViewById(R.id.editTextMTProveedor);
        direccion=(EditText)findViewById(R.id.editTextMDiproveedor);
        alias=(EditText)findViewById(R.id.editTextMAlias);

        mTableLayoutReporte=				(TableLayout)findViewById(R.id.tablelayout_modificarproveedor);

        // MOSTRAR LO QUE CONTIENE LAS VARIABLE
        objGestionDB.EditarProveedor(id_proveedor,contexto);
        nombre.setText(objGestionDB.nombres_proveedor);
        apellido.setText(objGestionDB.apellidos_proveedor);
        telefono.setText(objGestionDB.telefono_proveedor);
        direccion.setText(objGestionDB.direccion_proveedor);
        alias.setText(objGestionDB.alias_proveedor);

        sp_sexo=(Spinner)findViewById(R.id.spinnerMSexpaciente);
        sp_estado=(Spinner)findViewById(R.id.spinnerMEstadoProveedor);
        //objGestionDB.examen();
        cargarspinerSexo(id_proveedor);
        cargarspinerEstado(id_proveedor);



        // SACAR ID SEXO SELECCIONADO
        sp_sexo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // guardo en variable el id de lo seleccionado
                id_sexo = (((SpinnerObjectString) sp_sexo.getSelectedItem()).getId());

            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        // SACAR ID ESTADO SELECCIONADO
        sp_estado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // guardo en variable el id de lo seleccionado
                id_estado = (((SpinnerObjectString) sp_estado.getSelectedItem()).getId());

            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

            // crear consulta para generar la tabla
        CrearConsulta();

    }

    public void CrearConsulta(){

        mListaActividades=objGestionDB.obtenerActividadesGenerales(contexto);
        crearTabla();
    }



    private void crearTabla(){

        //Removemos el contenido anterior de la tabla en caso de que exista
        mTableLayoutReporte.removeAllViews();  //removeAllViewsInLayout ()
        mTableLayoutReporte.removeAllViewsInLayout();
        //Colocamo el nombre de la seleccion
        // mTextViewNombreSeleccion.setText(mNombreSeleccion)
//;
        for (RowReporteA fila : mListaActividades) {

            //Creamos una nueva instancica de fila para la tabla
            final TableRow tableRow = new TableRow(context);
            // ListView listview = new ListView(context);

            //  tableRow.width = 252;

            //Creamos el texview donde se mostrara la informacion
            TextView textViewNombreCompleto      = new TextView(context);
            TextView textViewSexo       = new TextView(context);
            TextView textViewAlias      = new TextView(context);
            TextView textViewDireccion  = new TextView(context);
            TextView textViewTelefono   = new TextView(context);
            TextView textViewEstado     = new TextView(context);
            TextView id                 = new TextView(context);


            //Colocamos la nombre a mostrar en la columna
            textViewNombreCompleto.setText((fila.getnombre()));
            textViewSexo.setText((fila.getsexo()));
            textViewAlias.setText(fila.getalias());
            textViewDireccion.setText(fila.getdireccion());
            textViewTelefono.setText(fila.gettelefono());
            textViewEstado.setText(fila.getestado());
            id.setText((fila.getid()));

            /*
               COLUMNA NOMBRE COMPLETO
            */

            //Asignamos el color de backgroud que tendra la primer columna de la tabla
            textViewNombreCompleto.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewNombreCompleto.setGravity(Gravity.CENTER);
            textViewNombreCompleto.setTextSize(15);
            textViewNombreCompleto.setTypeface(null, Typeface.BOLD);
            textViewNombreCompleto.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams nombreCompletoTableRowParams = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
            nombreCompletoTableRowParams.setMargins(1, 1, 1, 1);
            nombreCompletoTableRowParams.width = 199;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewNombreCompleto, nombreCompletoTableRowParams);

            // FIN COLUMNA NOMBRE


                    /*
                    COLUMNA SEXO
                     */
            //Asignamos el color
            textViewSexo.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewSexo.setGravity(Gravity.CENTER);
            textViewSexo.setTextSize(15);
            textViewSexo.setTypeface(null, Typeface.BOLD);
            textViewSexo.setHeight(50);


            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams SexoTableRowParams = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
            SexoTableRowParams.setMargins(1, 1, 1, 1);
            SexoTableRowParams.width = 100;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewSexo, SexoTableRowParams);

            //FIN COLUMNA SEXO.

                /*
                COLUMNA ALIAS
                 */
            textViewAlias.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));


            textViewAlias.setGravity(Gravity.CENTER);
            textViewAlias.setTextSize(15);
            textViewAlias.setTypeface(null, Typeface.BOLD);
            textViewAlias.setHeight(50);

            TableRow.LayoutParams AliasTableRowParams = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
            AliasTableRowParams.setMargins(1, 1, 1, 1);
            AliasTableRowParams.width = 100;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewAlias, AliasTableRowParams);

            // FIN COLUMNA ALIAS


            /*
            COLUMNA DIRECCION
             */
            //Asignamos el color de backgroud que tendra la primer columna de la tabla
            textViewDireccion.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewDireccion.setGravity(Gravity.CENTER);
            textViewDireccion.setTextSize(15);
            textViewDireccion.setTypeface(null, Typeface.BOLD);
            textViewDireccion.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams direccionTableRowParams = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
            direccionTableRowParams.setMargins(1, 1, 1, 1);
            direccionTableRowParams.width = 110;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewDireccion, direccionTableRowParams);
            // FIN COLUMNA DIRECCION


            /*
            columna telefono
             */
            textViewTelefono.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewTelefono.setGravity(Gravity.CENTER);
            textViewTelefono.setTextSize(15);
            textViewTelefono.setTypeface(null, Typeface.BOLD);
            textViewTelefono.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams telefonoTableRowParams = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
            telefonoTableRowParams.setMargins(1, 1, 1, 1);
            telefonoTableRowParams.width = 110;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewTelefono, telefonoTableRowParams);
            // fin columna telefono.




            /*
            columna  ESTADO.
             */
            textViewEstado.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewEstado.setGravity(Gravity.CENTER);
            textViewEstado.setTextSize(15);
            textViewEstado.setTypeface(null, Typeface.BOLD);
            textViewEstado.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams estadoTableRowParams = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
            estadoTableRowParams.setMargins(1, 1, 1, 1);
            estadoTableRowParams.width = 99;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewEstado, estadoTableRowParams);
            // columna estado.




            //Creamos el linear layout que contendra las actividades de la fecha
            LinearLayout layout = new LinearLayout(context);
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));







            //Recorremos toda la lista de actividades para colocarlas dentro del linearlayout
          /*  for (String actividad : fila.Actividades) {

                //Creamos las pripiedades del layout que debe tener el textview que muestra la actividad
                LinearLayout.LayoutParams layoutParamsActividad = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutParamsActividad.setMargins(1, 1, 1, 1);

                //Creamos el texview donde se muestra la actividad que se debe realizar en la fecha
                TextView textViewActividad = new TextView(context);

                //Colocamos el backgroud que mostrara la fila  que contiene la actividad
                textViewActividad.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

                //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
                textViewActividad.setGravity(Gravity.LEFT|Gravity.CENTER);
                textViewActividad.setPadding(10, 0, 0, 0);
                textViewActividad.setHeight(50);
                textViewActividad.setTextSize(15);

                //Colocamos la actividad a mostrar en el textview
                textViewActividad.setText(actividad);

                //Agregamos la ctividad al layout
                layout.addView(textViewActividad,layoutParamsActividad);
            }*/
            //Creamos los parametros de layout para la segunda columna de la tabla
          /*  TableRow.LayoutParams activdadTableRowParams = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
            activdadTableRowParams.setMargins(1, 1, 1, 1);
            activdadTableRowParams.width = 100;*/

            //-----------------------------

            // COLUMNA ALIAS


            //Agregamos las actividades a la fila de la tabla para que utilicen la segunda columna
            // tableRow.addView(layout, activdadTableRowParams);
            final String s_id 	    = id.getText().toString().trim();
            tableRow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {


                    tableRow.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_light));
                    // Toast.makeText(this, "---->"+s_id , Toast.LENGTH_LONG).show();
                    Intent i = new Intent(Modificar_Proveedor.this, modificarElimiarProveedor.class);
                    i.putExtra("s_id", s_id);
                    i.putExtra("var_user",nombreusuario);
                    // finish();
                    startActivity(i);


                    // modificarEliminar(s_id);



                }
            });

            //Agregamos la fila creada a la tabla de la pantalla

            mTableLayoutReporte.addView(tableRow);

        }

    }

    public void click_modificarproveedor(View modificar){



        String mnombres   = nombre.getText().toString().trim();
        String mapellidos = apellido.getText().toString().trim();
        String mtelefono  = telefono.getText().toString().trim();
        String mdireccion = direccion.getText().toString().trim();
        String malias     = alias.getText().toString().trim();

                //  String s_nombre    = nombre.getText().toString().trim();

        int respuesta=validar(mnombres,mapellidos,mtelefono,mdireccion,malias,id_sexo, id_estado);

        if(respuesta==1){
            // Toast.makeText(this, "mal ", Toast.LENGTH_LONG).show();
            errorcompletarcampo();
        }else {

           // Toast.makeText(this, "todo bien"+id_estado,Toast.LENGTH_LONG).show();
            objGestionDB.actualizarProveedor(contexto, id_proveedor, mnombres, mapellidos, mtelefono, mdireccion, malias, id_sexo, id_estado);
            actualizado();
        }
    }

    public void actualizado()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.confirmacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloconfirmacion);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeconfirmacion);
        contenido.setText("Proveedor Actualizado Correctamente");

        ((Button) customDialog.findViewById(R.id.btn_aceptarConfirmacion)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();
                    // i.putExtra("var_user",nombreusuario);


                Intent i= new Intent(Modificar_Proveedor.this,proveedor.class);
                i.putExtra("var_user",nombreusuario);
                finish();
                startActivity(i);

            }
        });



        customDialog.show();
    }




    public void errorcompletarcampo()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.errorcompletecampos);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloErrorUsuario);
        titulo.setText("ERROR!!");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeErrorUsuario);
        contenido.setText("Complete los Campos");

        ((Button) customDialog.findViewById(R.id.btn_aceptarErrorUsuario)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });



        customDialog.show();
    }

    public int validar(String mnombres, String mapellidos, String mtelefono, String mdireccion,String malias, int id_sexo , int id_estado){
        int respuesta=0;

        // NOMBRE
        if (mnombres.equals("")){

            respuesta=1;

        }

        //APELLIDO
        if (mapellidos.equals("")){

            respuesta=1;

        }
        // TELEFONO
        if (mtelefono.equals("")){

            respuesta=1;

        }
        // DIRECCION
        if (mdireccion.equals("")){

            respuesta=1;

        }

        // SEXO
        if (id_sexo==99){

            respuesta=1;

        }

        // ESTADO
        if (id_estado==99){

            respuesta=1;

        }

        // ALIAS
        if (malias.equals("")){

            respuesta=1;

        }
        return respuesta;
    }

    public void click_MenuPrincipalModiPro(View menu){



        Intent i= new Intent(Modificar_Proveedor.this, proveedor.class);
        i.putExtra("var_user",nombreusuario);
        finish();
        startActivity(i);
    }



    // CARGAR COMBO DE EXÁMEN --> SPINNER
    public void cargarspinerSexo(String id_proveedor) {



        objGestionDB.IDeSexo(id_proveedor, contexto);
        n_id_sexo=objGestionDB.idsexo;
        // Toast.makeText(this, "id del examen"+n_id_examen  , Toast.LENGTH_LONG).show();


        List<SpinnerObjectString> lables = objGestionDB.geteEditsexo(contexto);

        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(this, android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_sexo.setAdapter(dataAdapter);

        int pos=0;
        for(int i=0; i<lables.size(); i++){
            if(lables.get(i).getId()==n_id_sexo){
                pos=i;
            }
        }
        sp_sexo.setSelection(pos);
    }


    // CARGAR COMBO DE estado --> SPINNER
    public void cargarspinerEstado(String id_proveedor) {



        objGestionDB.idestado(id_proveedor, contexto);
        n_id_estado=objGestionDB.idestado;
        // Toast.makeText(this, "id del examen"+n_id_examen  , Toast.LENGTH_LONG).show();


        List<SpinnerObjectString> lables = objGestionDB.geteEditestado(contexto);

        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(this, android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_estado.setAdapter(dataAdapter);

        int pos=0;
        for(int i=0; i<lables.size(); i++){
            if(lables.get(i).getId()==n_id_estado){
                pos=i;
            }
        }
        sp_estado.setSelection(pos);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_modificar__proveedor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
