package com.example.david.utec.Clientes;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.david.utec.Principal.SpinnerObjectString;
import com.example.david.utec.Proveedor.proveedor;
import com.example.david.utec.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import db_gestion.BaseDeDatos;
import db_gestion.GestionDB;

public class Modificar_Clientes extends Activity {

    public SQLiteDatabase conexOpen;
    public Context context = this;
    public GestionDB objGestionDB ;
    public GestionDB obj;

    public Context contexto=this;

    String id_cliente, nombreusuario;

    private EditText nombre,apellido,direccion, telefono, sexo, codigo;

    Spinner sp_sexo,  sp_estado;
    int  id_sexo, id_estado,  n_id_sexo=0,  n_id_estado;

    // varibale para el dialogo
    Dialog customDialog=null;


    // para la tabla
    private TableLayout mTableLayoutReporte;
    private ArrayList<Tablaclientes> mListaActividades;
    private LinearLayout mLinearLayoutContenedorPrincipal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_modificar__clientes);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set content view AFTER ABOVE sequence (to avoid crash)
        this.setContentView(R.layout.activity_modificar__clientes);


        Bundle bundle = getIntent().getExtras();
        nombreusuario=bundle.getString("var_user");
        id_cliente=bundle.getString("s_id");

     //   Toast.makeText(this,"-->"+nombreusuario+id_cliente,Toast.LENGTH_LONG).show();



        BaseDeDatos objBaseDeDatos = new BaseDeDatos(this);
        try {
            objBaseDeDatos.createDataBase();
            // db2.openDataBase();
            //this.conexOpen = db2.myDataBase;
            this.objGestionDB = new GestionDB();// creo el objeto de la clase que gestiona la DB
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        sp_estado=(Spinner) findViewById(R.id.spinnerMEstadocliente);
        sp_sexo=(Spinner)findViewById(R.id.spinnerMSexcliente);

        nombre=(EditText) findViewById(R.id.editTextMnombrescliente);
        apellido=(EditText)findViewById(R.id.editTextMApellidoscliente);
        telefono=(EditText)findViewById(R.id.editTextMtelefonocliente);
        direccion=(EditText)findViewById(R.id.editTextMDireccioncliente);
        codigo=(EditText)findViewById(R.id.editTextMcodigocliente);

        mTableLayoutReporte=				(TableLayout)findViewById(R.id.tablelayout_Mclientes);

        // MOSTRAR LO QUE CONTIENE LAS VARIABLE
        objGestionDB.EditarCliente(id_cliente, contexto);
        nombre.setText(objGestionDB.nombres_cliente);
        apellido.setText(objGestionDB.apellidos_cliente);
        telefono.setText(objGestionDB.telefono_cliente);
        direccion.setText(objGestionDB.direccion_cliente);
        codigo.setText(objGestionDB.codigo_cliente);

        CrearConsulta();
        cargarspinerSexo(id_cliente);
        cargarspinerEstado(id_cliente);

        // SACAR ID SEXO SELECCIONADO
        sp_sexo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // guardo en variable el id de lo seleccionado
                id_sexo = (((SpinnerObjectString) sp_sexo.getSelectedItem()).getId());

            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });


        // SACAR ID ESTADO SELECCIONADO
        sp_estado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // guardo en variable el id de lo seleccionado
                id_estado = (((SpinnerObjectString) sp_estado.getSelectedItem()).getId());

            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

    }

    public void CrearConsulta(){

        mListaActividades=objGestionDB.generartablacliente(contexto);
        crearTabla();
    }

    private void crearTabla(){

        //Removemos el contenido anterior de la tabla en caso de que exista
        mTableLayoutReporte.removeAllViews();  //removeAllViewsInLayout ()
        mTableLayoutReporte.removeAllViewsInLayout();
        //Colocamo el nombre de la seleccion
        // mTextViewNombreSeleccion.setText(mNombreSeleccion)
//;
        for (Tablaclientes fila : mListaActividades) {

            //Creamos una nueva instancica de fila para la tabla
            final TableRow tableRow = new TableRow(context);
            // ListView listview = new ListView(context);

            //  tableRow.width = 252;

            //Creamos el texview donde se mostrara la informacion
            TextView textViewNombreCompleto      = new TextView(context);
            TextView textViewSexo       = new TextView(context);
            TextView textViewAlias      = new TextView(context);
            TextView textViewDireccion  = new TextView(context);
            TextView textViewTelefono   = new TextView(context);
            TextView textViewEstado     = new TextView(context);
            TextView id                 = new TextView(context);


            //Colocamos la nombre a mostrar en la columna
            textViewNombreCompleto.setText((fila.getnombre()));
            textViewSexo.setText((fila.getsexo()));
            textViewAlias.setText(fila.getcodigo());
            textViewDireccion.setText(fila.getdireccion());
            textViewTelefono.setText(fila.gettelefono());
            textViewEstado.setText(fila.getestado());
            id.setText((fila.getid()));

            /*
               COLUMNA NOMBRE COMPLETO
            */

            //Asignamos el color de backgroud que tendra la primer columna de la tabla
            textViewNombreCompleto.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewNombreCompleto.setGravity(Gravity.CENTER);
            textViewNombreCompleto.setTextSize(15);
            textViewNombreCompleto.setTypeface(null, Typeface.BOLD);
            textViewNombreCompleto.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams nombreCompletoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            nombreCompletoTableRowParams.setMargins(1, 1, 1, 1);
            nombreCompletoTableRowParams.width = 199;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewNombreCompleto, nombreCompletoTableRowParams);

            // FIN COLUMNA NOMBRE


                    /*
                    COLUMNA SEXO
                     */
            //Asignamos el color
            textViewSexo.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewSexo.setGravity(Gravity.CENTER);
            textViewSexo.setTextSize(15);
            textViewSexo.setTypeface(null, Typeface.BOLD);
            textViewSexo.setHeight(50);


            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams SexoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            SexoTableRowParams.setMargins(1, 1, 1, 1);
            SexoTableRowParams.width = 100;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewSexo, SexoTableRowParams);

            //FIN COLUMNA SEXO.

                /*
                COLUMNA ALIAS
                 */
            textViewAlias.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));


            textViewAlias.setGravity(Gravity.CENTER);
            textViewAlias.setTextSize(15);
            textViewAlias.setTypeface(null, Typeface.BOLD);
            textViewAlias.setHeight(50);

            TableRow.LayoutParams AliasTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            AliasTableRowParams.setMargins(1, 1, 1, 1);
            AliasTableRowParams.width = 100;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewAlias, AliasTableRowParams);

            // FIN COLUMNA ALIAS


            /*
            COLUMNA DIRECCION
             */
            //Asignamos el color de backgroud que tendra la primer columna de la tabla
            textViewDireccion.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewDireccion.setGravity(Gravity.CENTER);
            textViewDireccion.setTextSize(15);
            textViewDireccion.setTypeface(null, Typeface.BOLD);
            textViewDireccion.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams direccionTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            direccionTableRowParams.setMargins(1, 1, 1, 1);
            direccionTableRowParams.width = 110;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewDireccion, direccionTableRowParams);
            // FIN COLUMNA DIRECCION


            /*
            columna telefono
             */
            textViewTelefono.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewTelefono.setGravity(Gravity.CENTER);
            textViewTelefono.setTextSize(15);
            textViewTelefono.setTypeface(null, Typeface.BOLD);
            textViewTelefono.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams telefonoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            telefonoTableRowParams.setMargins(1, 1, 1, 1);
            telefonoTableRowParams.width = 110;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewTelefono, telefonoTableRowParams);
            // fin columna telefono.




            /*
            columna  ESTADO.
             */
            textViewEstado.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewEstado.setGravity(Gravity.CENTER);
            textViewEstado.setTextSize(15);
            textViewEstado.setTypeface(null, Typeface.BOLD);
            textViewEstado.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams estadoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            estadoTableRowParams.setMargins(1, 1, 1, 1);
            estadoTableRowParams.width = 100;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewEstado, estadoTableRowParams);
            // columna estado.




            //Creamos el linear layout que contendra las actividades de la fecha
            LinearLayout layout = new LinearLayout(context);
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));






            //Agregamos las actividades a la fila de la tabla para que utilicen la segunda columna
            // tableRow.addView(layout, activdadTableRowParams);
            final String s_id 	    = id.getText().toString().trim();
            tableRow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {


                    tableRow.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_light));
                    // Toast.makeText(this, "---->"+s_id , Toast.LENGTH_LONG).show();
                    Intent i = new Intent(Modificar_Clientes.this, Modificar_Eliminar_Cliente.class);
                    i.putExtra("s_id", s_id);
                    i.putExtra("var_user",nombreusuario);
                    // finish();
                    startActivity(i);


                    // modificarEliminar(s_id);



                }
            });

            //Agregamos la fila creada a la tabla de la pantalla

            mTableLayoutReporte.addView(tableRow);

        }

    }



    // CARGAR COMBO DE EXÁMEN --> SPINNER
    public void cargarspinerSexo(String id_cliente) {



        objGestionDB.EditarCliente(id_cliente, contexto);
        n_id_sexo=objGestionDB.idsexo_cliente;
        // Toast.makeText(this, "id del examen"+n_id_examen  , Toast.LENGTH_LONG).show();


        List<SpinnerObjectString> lables = objGestionDB.geteEditsexo(contexto);

        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(this, android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_sexo.setAdapter(dataAdapter);

        int pos=0;
        for(int i=0; i<lables.size(); i++){
            if(lables.get(i).getId()==n_id_sexo){
                pos=i;
            }
        }
        sp_sexo.setSelection(pos);
    }


    // CARGAR COMBO DE estado --> SPINNER
    public void cargarspinerEstado(String id_cliente) {



        objGestionDB.EditarCliente(id_cliente, contexto);
        n_id_estado=objGestionDB.idestado_cliente;
        // Toast.makeText(this, "id del examen"+n_id_examen  , Toast.LENGTH_LONG).show();


        List<SpinnerObjectString> lables = objGestionDB.geteEditestado(contexto);

        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(this, android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_estado.setAdapter(dataAdapter);

        int pos=0;
        for(int i=0; i<lables.size(); i++){
            if(lables.get(i).getId()==n_id_estado){
                pos=i;
            }
        }
        sp_estado.setSelection(pos);
    }


    public void editarCliente( View editar_cliente){


        String s_nombre      = nombre.getText().toString().trim();
        String s_apellidos  = apellido.getText().toString().trim();
        String s_telefono   = telefono.getText().toString().trim();
        String s_direccion  = direccion.getText().toString().trim();
        String s_codigo     = codigo.getText().toString().trim();

       int respuesta= validar(s_nombre,s_apellidos,s_telefono, s_direccion,s_codigo, id_sexo, id_estado );


        if (respuesta==0){

           // Toast.makeText(this,"bien",Toast.LENGTH_SHORT).show();
            objGestionDB.modificarcliente(contexto,s_nombre,s_apellidos,s_telefono, s_direccion,s_codigo, id_sexo, id_estado, id_cliente);
                confirmar_modificaion();


        }else{
          // Toast.makeText(this,"complete los campos",Toast.LENGTH_SHORT).show();

            errorcompletarcampo();
        }

    }

    public int validar (String  s_nombre, String s_apellidos, String s_telefono, String s_direccion, String s_codigo, int  id_sexo, int id_estado){
       int  respuesta=0;

        // nombres
        if(s_nombre.equals("")){
            respuesta=1;

        }
        // apellidos
        if(s_apellidos.equals("")){

            respuesta=1;

        }

        // telefono
        if(s_telefono.equals("")){

            respuesta=1;

        }


        // direccion

        if(s_direccion.equals("")){

            respuesta=1;

        }

        // código
      if(s_codigo.equals("")){

            respuesta=1;

        }

        // sexo
        if(id_sexo==99){

            respuesta=1;

        }

        // estado
        if(id_estado==99){

            respuesta=1;

        }


        return  respuesta;
    }


    public void errorcompletarcampo()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.errorcompletecampos);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloErrorUsuario);
        titulo.setText("ERROR!!");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeErrorUsuario);
        contenido.setText("Complete los Campos, Para Realizar la Modificación");

        ((Button) customDialog.findViewById(R.id.btn_aceptarErrorUsuario)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });



        customDialog.show();
    }


    public void confirmar_modificaion()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.confirmacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloconfirmacion);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeconfirmacion);
        contenido.setText("Cliente Actualizado Correctamente");

        ((Button) customDialog.findViewById(R.id.btn_aceptarConfirmacion)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();
                // i.putExtra("var_user",nombreusuario);


                Intent i= new Intent(Modificar_Clientes.this,Clientes.class);
                i.putExtra("var_user",nombreusuario);
                finish();
                startActivity(i);

            }
        });



        customDialog.show();
    }


    public void regresaraclientes(View menu){



        Intent i= new Intent(Modificar_Clientes.this, Clientes.class);
        i.putExtra("var_user", nombreusuario);
        finish();
        startActivity(i);
      //  Toast.makeText(this,"regresar",Toast.LENGTH_LONG).show();
    }

}
